# this code is server protocol 

#!/usr/bin/python2.7
from twisted.internet import protocol, reactor
   
class Echo(protocol.Protocol):
    def dataRecived(self, data):
        self.transport.write(data)
   
   
class EchoFactory(protocol.Factory):
    def buildProtocol(self, addr):
        return Echo()
  
  
  reactor.listenTCP(8000, EchoFactory())
  reactor.run()
